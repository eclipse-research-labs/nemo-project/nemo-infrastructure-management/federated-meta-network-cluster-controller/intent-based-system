# © 2024 Telefónica Innovación Digital

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import json
import logging

from devtools import pprint
from pydantic import ValidationError

from intent_engine.catalogue.abstract_library import abstract_library
from intent_engine.core.ib_model import IntentModel,IntentNrm, jsonable_model_encoder

logger = logging.getLogger(__name__)

class l2vpn(abstract_library):
    """
    Abstract class created as 
    """
    def __init__(self):
        decision_tree={
           "cloud_continuum" : {
               "DELIVER":{
                   "L2VPN":"l2vpn"
                    },
                "mininet": "mininet_controller"
                }
        }
        params={
            "context_uuid":"admin",
            "service_uuid":"l2-acl-svc-intent",
            "node_src":"5.5.5.5",
            "endpoint_src":"GigabitEthernet0/0/0/1", #Endpoint to tfs format?
            "node_dst":"3.3.3.3",
            "endpoint_dst":"GigabitEthernet0/0/0/1",
            "bandwidth":"10.0",
            "latency":"15.2",
            "vlan_id":999,
            "circuit_id":"999",
            "ni_name":"ninametfssssss"
        }
        super().__init__(module_name="l2vpn",isILU=False,params=params,decision_tree=decision_tree)
        self.__params=params
    
    def generate_subintent(self,intent_model : IntentModel):
        intent=intent_model.get_intent()
        logger.debug("Generating L2VPN TFS subintent...")
        for i,exp in enumerate(intent.intentExpectations):
            if exp.expectationVerb == 'DELIVER':
                # new_dict=exp.dict()
                # new_dict['expectationObject']['objectType']='L2VPN_TFS'
                intent_dict=intent.dict(exclude_defaults=True)
                intent_dict['intentExpectations'][i]['expectationObject']['objectType']='L2VPN_TFS'
                # pprint(new_dict)
                # IntentNrm.NewTFSL2VPNIntentExpectation(**new_dict)
                pprint(jsonable_model_encoder(intent_dict))
                subintent=IntentNrm.IntentMncc(**jsonable_model_encoder(intent_dict))
        # TODO: meter la nbi de TFS como parte del context para el subintent
        logger.debug("L2VPN subintent : %s",subintent)
        intent_model.set_intent(subintent)
        return intent_model
    
    def translator(self,subintent : IntentModel) -> tuple[list , str]:
        # TODO: cambiar el connect_type dependiendo del intent de  tfs session
        # FIXME: This is deprecated to tfs_controller
        exec_params=[]
        logger.info("Translating L2VPN...")
        logger.debug("debug L2VPN...")
        intent = subintent.get_intent()
        for exp in intent.intentExpectations:
            exp_verb=exp.expectationVerb
            logger.debug("expectation case %s",exp_verb)
            match exp_verb:
                case "DELIVER":
                    try:
                        IntentNrm.L2VPNExpectations(**(exp.dict()))
                    except ValidationError as exc:
                        logger.warning("Assurance error for L2SM NewNetworkExpectation:  %s", exc)
                    exp_obj=exp.expectationObject
                    logger.debug("request case obj: %s",exp_obj)
                    exp_type=exp_obj.objectType
                    match exp_type:
                        case "L2VPN":
                            for obj_ctx in exp_obj.get_contexts():
                                # Loop ctx inside obj
                                logger.debug("objectctx case %s: ",obj_ctx)
                                att=obj_ctx.get_attribute()
                                match att:
                                    case "node_src":
                                        logger.debug("node_src case %s",obj_ctx.get_value_range())
                                        self.__params['node_src']=obj_ctx.get_value_range()
                                        logger.debug("params after %s",self.__params)
                                    case "node_dst":
                                        logger.debug("node_dst case")
                                        self.__params['node_dst']=obj_ctx.get_value_range()
                                    case "vlan_id":
                                        logger.debug("vlan_id case")
                                        self.__params['vlan_id']=obj_ctx.get_value_range()
                    for trg_ctx in exp.get_target():
                        # Loop trg inside exp
                        att=trg_ctx.get_attribute()
                        match att:
                            case "signature":
                                logger.debug("signature case")
                        trg_ctx=trg_ctx.get_context()
                        if trg_ctx:
                            for ctx in trg_ctx:
                                # Loop ctx inside trg inside exp
                                att=ctx.get_attribute()
                                match att:
                                    case "signature":
                                        logger.debug("signature_trg_ctx case")

        # esto debería context del intent
        match subintent.get_context().get_name():
            case "tfs_controller":
                logger.debug("intent context tfs controller case")
                match subintent.get_context().get_attribute():
                    case "l2vpn":
                        logger.debug("intent context att l2vpn case")
                        exec_params={
                            "url":subintent.get_context().get_value_range(),
                            "headers" : {'Content-Type': 'application/json'}
                        }
                    case "url":
                        logger.debug("intent context att ulr case")
                        exec_params={
                            "url":subintent.get_context().get_value_range(),
                            "headers" : {'Content-Type': 'multipart/form-data'},
                            "connect_type" : 'get'
                        }
        return [self.vpnl2_schema(),exec_params],"tfs_connector"

    def get_blueprints(self):
        blue_prints=[{},{}]
        return blue_prints
    
    def create_ilu(self,intent : IntentModel) -> IntentModel:
        # TODO: puede que esto no tenga sentido porq el classifier debería classificarlo como 
        # Deprecated
        # tfs_controller?    

        subintent=intent
        
        exec_params=[]
        logger.info("Creating L2VPN ILU...")
        logger.debug("debug L2VPN create ILU...")
        for exp in subintent.get_expectations():
            exp_verb=exp.get_verb()
            logger.debug("expectation case %s",exp_verb)
            match exp_verb:
                # Expectation verb
                case "request":
                    exp_obj=exp.get_object()
                    logger.debug("request case obj: %s",exp_obj)
                    exp_type=exp_obj.get_type()
                    # Expectation type
                    match exp_type:
                        # Object type of intent, what(target) is expected from object?
                        case "l2vpn":
                            # l2vpn objects that are installed
                            # in this case l2vpn tfs_objects
                            for obj_ctx in exp_obj.get_contexts():
                                logger.debug("objectctx case %s: ",obj_ctx)
                                att=obj_ctx.get_attribute()
                                match att:
                                    case "node_src":
                                        logger.debug("node_src case %s",obj_ctx.get_value_range())

                                    case "node_dst":
                                        logger.debug("node_dst case")

                                    case "vlan_id":
                                        logger.debug("vlan_id case")
  
                    for trg_ctx in exp.get_target():
                        # Loop trg inside exp
                        att=trg_ctx.get_attribute()
                        match att:
                            case "signature":
                                logger.debug("signature case")
                        trg_ctx=trg_ctx.get_context()
                        if trg_ctx:
                            for ctx in trg_ctx:
                                # Loop ctx inside trg inside exp
                                att=ctx.get_attribute()
                                match att:
                                    case "signature":
                                        logger.debug("signature_trg_ctx case")

        # Context del Intent
        # Management purposes
        match subintent.get_context().get_name():
            case "scheduling":
                logger.debug("intent context tfs controller case")
                match subintent.get_context().get_attribute():
                    case "state":
                        logger.debug("intent context att state case")
                    case "url":
                        logger.debug("intent context att ulr case")

        return subintent

    def ietf_l2vpn_schema(self):
        ietf_l2vpn={
            
        }
        return 
    def vpnl2_schema(self):

        vpn_descriptor={
                        "services": [
                            {
                                "service_id": {
                                    "context_id": {"context_uuid": {"uuid": self.__params['context_uuid']}},
                                    "service_uuid": {"uuid": self.__params['service_uuid']}
                                },
                                "service_type": 2,
                                "service_status": {"service_status": 1},
                                "service_endpoint_ids": [
                                    {"device_id": {"device_uuid": {"uuid": self.__params['node_src']}}, "endpoint_uuid": {"uuid": "0/0/1-"+self.__params['endpoint_src']}},
                                    {"device_id": {"device_uuid": {"uuid": self.__params['node_dst']}}, "endpoint_uuid": {"uuid": "0/0/1-"+self.__params['endpoint_dst']}}
                                ],
                                "service_constraints": [
                                    {"custom": {"constraint_type": "bandwidth[gbps]", "constraint_value": self.__params['bandwidth']}},
                                    {"custom": {"constraint_type": "latency[ms]", "constraint_value": self.__params['latency']}}
                                ],
                                "service_config": {"config_rules": [
                                    {"action": 1, "custom": {"resource_key": "/settings", "resource_value": {
                                    }}},
                                    {"action": 1, "custom": {"resource_key": "/device["+self.__params['node_src']+"]/endpoint[0/0/1-"+self.__params['endpoint_src']+"]/settings", "resource_value": {
                                        "sub_interface_index": 0,
                                        "ni_name":self.__params['ni_name'],
                                        "vlan_id": int(self.__params['vlan_id']),
                                        "circuit_id": self.__params['circuit_id'],
                                        "remote_router":self.__params['node_dst']
                                    }}},
                                    {"action": 1, "custom": {"resource_key": "/device["+self.__params['node_dst']+"]/endpoint[0/0/1-"+self.__params['endpoint_dst']+"]/settings", "resource_value": {
                                        "sub_interface_index": 0,
                                        "ni_name":self.__params['ni_name'],
                                        "vlan_id": int(self.__params['vlan_id']),
                                        "circuit_id": self.__params['circuit_id'],
                                        "remote_router":self.__params['node_src']
                                    }}}
                                ]}
                            }
                        ]
                    }

        return vpn_descriptor
