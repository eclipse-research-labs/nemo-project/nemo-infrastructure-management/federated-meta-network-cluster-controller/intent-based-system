# © 2024 Telefónica Innovación Digital

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from http.server import BaseHTTPRequestHandler, HTTPServer
# import SocketServer
import simplejson
import random
import yaml

class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        with open("index.html", "r") as f:
            self.wfile.write(f.read().encode('utf-8'))

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        self._set_headers()
        print("in post method")
        self.data_string = self.rfile.read(int(self.headers['Content-Length']))
        content_type = self.headers['Content-type']
        
        if content_type == 'application/x-yaml':
            try:
                data = yaml.safe_load(self.data_string)
            except yaml.YAMLError as e:
                self.send_response(400)
                self.end_headers()
                self.wfile.write(f"Invalid YAML data: {e}".encode('utf-8'))
                return
        elif content_type == 'application/json':
            try:
                data = simplejson.loads(self.data_string)
            except simplejson.JSONDecodeError as e:
                self.send_response(400)
                self.end_headers()
                self.wfile.write(f"Invalid JSON data: {e}".encode('utf-8'))
                return
        else:
            self.send_response(415)
            self.end_headers()
            self.wfile.write("Unsupported Media Type".encode('utf-8'))
            return

        print("{}".format(data))
        self.send_response(200)
        self.end_headers()

        return


def run(server_class=HTTPServer, handler_class=S, port=8080):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print('Starting httpd...')
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()