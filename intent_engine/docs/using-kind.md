# How to deploy in kind nodes 

1. Build the images
```
docker build intent_engine/ -t kind-registry:5000/nemometaos/mncc-ibs:v0.0.2
```
2. Load images into kind cluster registry
```
kind load docker-image kind-registry:5000/nemometaos/mncc-ibs:v0.0.2 --name meta-cluster
```
3. Change manifest accordingly to image tag
```yaml
intent_engine/manifest.yaml
...
containers:
      - name: intent-engine
        image: kind-registry:5000/nemometaos/mncc-ibs:v0.0.2
        imagePullPolicy: IfNotPresent
...
```
4. Run the IBS
```
kubectl apply -f intent-based-system/intent_engine/manifest.yaml --context kind-meta-cluster
```
4. Check pod is listening correctly to the queue
```
kubectl logs -n nemo-net mncc-ibs-<...> --context kind-meta-cluster -f
...
[INFO - 11:03:18] intent_engine.executioners.rabbitMQ_recv: [Thread-1 (reciver)]  [*] Waiting for logs. To exit press CTRL+C in: mncc ['create-network-paths']
```
5. Send the intent to the RabbitMQ queue, this can be done in another machine.

```
<!-- python pika installation might be required -->
cd intent-based-system/intent_engine
python3.10 executioners/rabbitMQ_emit.py
```

> [!NOTE]
> The receiver queue is hardcoded in the script. 